import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * Created by kaeltas on 13.02.15.
 */
public class RandomizedQueue<Item> implements Iterable<Item> {

    private Item [] arrayQueue;
    private int size = 0;

    // construct an empty randomized queue
    public RandomizedQueue() {
        arrayQueue = (Item []) new Object[2];
    }

    // is the queue empty?
    public boolean isEmpty() {
        return (size == 0);
    }

    // return the number of items on the queue
    public int size() {
        return size;
    }

    // add the item
    public void enqueue(Item item) {
        if (item == null) {
            throw new NullPointerException();
        }

        if (size() == arrayQueue.length) {
            Item [] newArrQueue = (Item []) new Object [arrayQueue.length * 2];
            System.arraycopy(arrayQueue, 0, newArrQueue, 0, arrayQueue.length);
            arrayQueue = newArrQueue;
            //arrayQueue = Arrays.copyOf(arrayQueue, arrayQueue.length * 2);
        }
        arrayQueue[size++] = item;

    }

    // remove and return a random item
    public Item dequeue() {
        if (size() == 0) {
            throw new NoSuchElementException();
        }

        int rnd = StdRandom.uniform(size());
        Item item = arrayQueue[rnd];
        arrayQueue[rnd] = arrayQueue[--size];
        arrayQueue[size] = null;

        if (size() < arrayQueue.length/4) {
            Item [] newArrQueue = (Item []) new Object [arrayQueue.length/2];
            System.arraycopy(arrayQueue, 0, newArrQueue, 0, arrayQueue.length/2);
            arrayQueue = newArrQueue;
            //arrayQueue = Arrays.copyOf(arrayQueue, arrayQueue.length/2);
        }

        return item;
    }

    // return (but do not remove) a random item
    public Item sample() {
        if (size() == 0) {
            throw new NoSuchElementException();
        }

        return arrayQueue[StdRandom.uniform(size())];
    }

    // return an independent iterator over items in random order
    @Override
    public Iterator<Item> iterator() {
        return new RandomizedQueueIterator();
    }

    private class RandomizedQueueIterator implements Iterator<Item> {
        private int cursor;
        private int [] randomOrder;

        public RandomizedQueueIterator() {
            this.cursor = 0;

            randomOrder = new int[size()];
            for (int i = 0; i < randomOrder.length; i++) {
                randomOrder[i] = i;
            }

            StdRandom.shuffle(randomOrder);

        }

        @Override
        public boolean hasNext() {
            return cursor < randomOrder.length;
        }

        @Override
        public Item next() {
            if (cursor == randomOrder.length) {
                throw new NoSuchElementException();
            }
            return arrayQueue[randomOrder[cursor++]];
        }

        @Override
        public void remove() {
            throw new UnsupportedOperationException();
        }
    }

    // unit testing
    public static void main(String[] args) {

        /*RandomizedQueue<Integer> randomizedQueue = new RandomizedQueue<Integer>();
        randomizedQueue.enqueue(10);
        randomizedQueue.enqueue(20);
        randomizedQueue.enqueue(30);
        randomizedQueue.enqueue(40);
        randomizedQueue.enqueue(50);
        randomizedQueue.enqueue(60);
        randomizedQueue.enqueue(70);
        randomizedQueue.enqueue(80);
        randomizedQueue.enqueue(90);
        randomizedQueue.enqueue(100);
        randomizedQueue.enqueue(110);

        System.out.println("samples");
        System.out.println(randomizedQueue.sample());
        System.out.println(randomizedQueue.sample());
        System.out.println(randomizedQueue.sample());
        System.out.println("---");



        for (Integer item : randomizedQueue) {
            System.out.println(item);
        }

        System.out.println("---");

        for (Integer item : randomizedQueue) {
            System.out.println(item);
        }*/

    }
}
