import java.util.NoSuchElementException;

/**
 * Created by kaeltas on 13.02.15.
 */
public class Subset {

    public static void main(String[] args) {
        int k = Integer.valueOf(args[0]);

        RandomizedQueue<String> randomizedQueue = new RandomizedQueue<String>();

        String str;
        while (true) {
            try {
                str = StdIn.readString();
            } catch (NoSuchElementException e) {
                break;
            }
            randomizedQueue.enqueue(str);
        }

        for (String item : randomizedQueue) {
            if (k-- <= 0) {
                break;
            }
            System.out.println(item);
        }

    }

}
