import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * Created by kaeltas on 13.02.15.
 */
public class Deque<Item> implements Iterable<Item> {
    private class Node {
        private Item item;
        private Node next;
        private Node prev;

        public Item getItem() {
            return item;
        }

        public void setItem(Item newItem) {
            this.item = newItem;
        }

        public Node getNext() {
            return next;
        }

        public void setNext(Node newNext) {
            this.next = newNext;
        }

        public Node getPrev() {
            return prev;
        }

        public void setPrev(Node newPrev) {
            this.prev = newPrev;
        }
    }

    private Node first;
    private Node last;
    private int size = 0;

    // construct an empty deque
    public Deque() {

    }

    // is the deque empty?
    public boolean isEmpty() {
        return (first == null) && (first == last);

    }

    // return the number of items on the deque
    public int size() {
        return size;
    }

    // add the item to the front
    public void addFirst(Item item) {
        if (item == null) {
            throw new NullPointerException();
        }

        size++;

        Node newNode = new Node();
        newNode.setItem(item);
        newNode.setNext(first);
        newNode.setPrev(null);
        if (first != null) {
            first.setPrev(newNode);
        }
        first = newNode;

        if (size() == 1) {
            last = newNode;
        }

    }

    // add the item to the end
    public void addLast(Item item) {
        if (item == null) {
            throw new NullPointerException();
        }

        size++;

        Node newNode = new Node();
        newNode.setItem(item);
        newNode.setNext(null);
        newNode.setPrev(last);
        if (last != null) {
            last.setNext(newNode);
        }
        last = newNode;

        if (size() == 1) {
            first = newNode;
        }

    }

    // remove and return the item from the front
    public Item removeFirst() {
        if (size() == 0) {
            throw new NoSuchElementException();
        }

        Node node = first;
        first = first.getNext();
        if (first != null) {
            first.setPrev(null);
        }

        size--;

        if (size() == 0) {
            last = null;
        }

        return node.getItem();
    }

    // remove and return the item from the end
    public Item removeLast() {
        if (size() == 0) {
            throw new NoSuchElementException();
        }

        Node node = last;
        last = last.getPrev();
        if (last != null) {
            last.setNext(null);
        }

        size--;
        if (size() == 0) {
            first = null;
        }

        return node.getItem();
    }

    // return an iterator over items in order from front to end
    public Iterator<Item> iterator() {
        return new DequeIterator();
    }

    private class DequeIterator implements Iterator<Item> {
        private Node cursor = first;

        @Override
        public boolean hasNext() {
            return cursor != null /*&& cursor.getNext() != null*/;
        }

        @Override
        public Item next() {
            if (cursor == null) {
                throw new NoSuchElementException();
            }

            Item item = cursor.getItem();
            cursor = cursor.getNext();

            return item;
        }

        @Override
        public void remove() {
            throw new UnsupportedOperationException();
        }
    }


    // unit testing
    public static void main(String[] args) {

        /*Deque<Integer> deque = new Deque<Integer>();
        *//*deque.addFirst(1);
        deque.addFirst(2);
        deque.addFirst(3);*//*

        deque.addLast(10);
        deque.addLast(20);
        deque.addLast(30);
        deque.addLast(40);
        deque.addLast(50);

        System.out.println("Size="+deque.size());

        System.out.println(deque.removeLast());
        System.out.println(deque.removeLast());
        System.out.println(deque.removeLast());
        System.out.println("Size="+deque.size());
        System.out.println(deque.removeLast());
        System.out.println(deque.removeLast());
        System.out.println("---");

        for (Integer item : deque) {
            System.out.println(item);
        }*/

    }


}
