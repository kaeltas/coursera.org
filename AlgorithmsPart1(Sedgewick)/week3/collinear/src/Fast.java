import java.util.*;

/**
 * Created by kaeltas on 14.02.15.
 */
public class Fast {

    private int numberOfPoints;
    private List<Point> points = new ArrayList<Point>();

    private void readPointsFromFile(String filename) {
        In in = new In(filename);
        numberOfPoints = in.readInt();

        for (int i = 0; i < numberOfPoints; i++) {
            points.add(new Point(in.readShort(), in.readShort()));
        }

        //System.out.println("not sorted " + points);
    }

    private void drawPoints() {
        StdDraw.setXscale(0, 32768);
        StdDraw.setYscale(0, 32768);
        for (Point p : points) {
            p.draw();
        }
    }



    private void printAndDrawLineSegments() {
        List<Point> copyPoints = new ArrayList<Point>(points);
        for (int i = 0; i < copyPoints.size(); i++) {
            Point p = copyPoints.get(i); //origin point for this iteration
            Point[] ar = copyPoints.toArray(new Point[0]);
            Arrays.sort(ar, p.SLOPE_ORDER);

            List<Point> pointsInSegment = new ArrayList<Point>();
            pointsInSegment.add(p);
            double slope = p.slopeTo(ar[0]);
            pointsInSegment.add(ar[0]);
            for (int j = 1; j < ar.length; j++) {
                //if slope is the same - add current point to pointInSegment
                if (slope == p.slopeTo(ar[j])) {
                    pointsInSegment.add(ar[j]);
                }
                //if slope is different for next point or we have got last point
                if (slope != p.slopeTo(ar[j]) || j == ar.length-1) {
                    if (pointsInSegment.size() >= 4) { //check if we found a segment of 4 or more points
                        Point[] arResult = pointsInSegment.toArray(new Point[0]);
                        Arrays.sort(arResult);

                        //print and draw line segment, only if current(origin) point is the smallest one
                        if (p.compareTo(arResult[0]) == 0) {
                            for (int k = 0; k < arResult.length; k++) {
                                System.out.print(arResult[k]);
                                if (k < arResult.length - 1) {
                                    System.out.print(" -> ");
                                }
                            }
                            System.out.println();

                            //take min and max point for draw Line to
                            Point minPoint = arResult[0];
                            Point maxPoint = arResult[arResult.length - 1];
                            minPoint.drawTo(maxPoint);
                        }
                    }
                    //reinitialize list for collecting new lineSegment
                    pointsInSegment = new ArrayList<Point>();
                    pointsInSegment.add(p);
                    slope = p.slopeTo(ar[j]);
                    pointsInSegment.add(ar[j]);
                }
            }

        }

    }

    public static void main(String[] args) {
        Fast fast = new Fast();
        fast.readPointsFromFile(args[0]);

        fast.drawPoints();
        fast.printAndDrawLineSegments();

    }

}
