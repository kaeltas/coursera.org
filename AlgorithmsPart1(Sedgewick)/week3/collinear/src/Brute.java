import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by kaeltas on 14.02.15.
 */
public class Brute {

    private int numberOfPoints;
    private List<Point> points = new ArrayList<Point>();

    private void readPointsFromFile(String filename) {
        In in = new In(filename);
        numberOfPoints = in.readInt();

        for (int i = 0; i < numberOfPoints; i++) {
            points.add(new Point(in.readShort(), in.readShort()));
        }

        //System.out.println(points);
    }

    private void drawPoints() {
        StdDraw.setXscale(0, 32768);
        StdDraw.setYscale(0, 32768);
        for (Point p : points) {
            p.draw();
        }
    }

    private void printAndDrawLineSegments() {
        Point [] ar = points.toArray(new Point[0]);
        Arrays.sort(ar);
        List<Point> copyPoints = Arrays.asList(ar);

        for (int i = 0; i < numberOfPoints; i++) {
            Point p1 = copyPoints.get(i);
            for (int j = i+1; j < numberOfPoints; j++) {
                Point p2 = copyPoints.get(j);
                for (int k = j+1; k < numberOfPoints; k++) {
                    Point p3 = copyPoints.get(k);
                    for (int l = k+1; l < numberOfPoints; l++) {
                        Point p4 = copyPoints.get(l);

                        if (p1.slopeTo(p2) == p2.slopeTo(p3) && p2.slopeTo(p3) == p3.slopeTo(p4)) {
                            System.out.println(p1 + " -> " + p2 + " -> " + p3 + " -> " + p4);

                            //draw Line to
                            Point maxPoint = max(max(max(p1, p2), p3), p4);
                            Point minPoint = min(min(min(p1, p2), p3), p4);
                            minPoint.drawTo(maxPoint);
                        }

                    }
                }
            }
        }

    }

    private Point min(Point p1, Point p2) {
        if (p1.compareTo(p2) < 0) {
            return p1;
        } else {
            return p2;
        }
    }

    private Point max(Point p1, Point p2) {
        if (p1.compareTo(p2) > 0) {
            return p1;
        } else {
            return p2;
        }
    }
    
    public static void main(String[] args) {
        Brute brute = new Brute();
        brute.readPointsFromFile(args[0]);

        brute.drawPoints();
        brute.printAndDrawLineSegments();

    }

    
}
