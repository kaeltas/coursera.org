import java.util.LinkedList;
import java.util.List;

/**
 * Created by kaeltas on 27.02.15.
 */
public class KdTree {

    private class Node {
        public static final boolean HORIZONTAL = true;
        public static final boolean VERTICAL = false;

        private Point2D point;
        private RectHV boundRect;
        private boolean orientation;
        private Node left;
        private Node right;

        public Node(Point2D point, boolean orientation) {
            this.point = point;
            this.orientation = orientation;
        }

        public Node(Point2D point, boolean orientation, RectHV boundRect) {
            this.point = point;
            this.orientation = orientation;
            this.boundRect = boundRect;
        }

        public Point2D getPoint() {
            return point;
        }

        public void setPoint(Point2D point) {
            this.point = point;
        }

        public RectHV getBoundRect() {
            return boundRect;
        }

        public void setBoundRect(RectHV boundRect) {
            this.boundRect = boundRect;
        }

        public int compareToPoint(Point2D that) {
            if (orientation == VERTICAL) {
                return (int) (point.x() - that.x());
            } else if (orientation == HORIZONTAL) {
                return (int) (point.y() - that.y());
            }
            return 0; //make compiler happy
        }
    }

    private Node root;
    private int size;

    // construct an empty set of points
    public KdTree() {

    }

    // is the set empty?
    public boolean isEmpty() {
        return size == 0;
    }

    // number of points in the set
    public int size() {
        return size;
    }

    // add the point to the set (if it is not already in the set)
    public void insert(Point2D p) {
        if (p == null) {
            throw new NullPointerException();
        }

        root = insert(root, p, Node.VERTICAL, 0, 0, 1, 1);
    }

    private Node insert(Node node, Point2D p, boolean orientation, RectHV boundRect) {
        if (node == null) {
            size++;
            return new Node(p, orientation, boundRect);
        }

        if (!node.getPoint().equals(p)) {
            if (node.orientation == Node.VERTICAL) {
                if (p.x() < node.getPoint().x()) {
                    RectHV newBound = new RectHV(boundRect.xmin(), boundRect.ymin(), node.getPoint().x(), boundRect.ymax());
                    node.left = insert(node.left, p, Node.HORIZONTAL, newBound);
                } else {
                    RectHV newBound = new RectHV(node.getPoint().x(), boundRect.ymin(), boundRect.xmax(), boundRect.ymax());
                    node.right = insert(node.right, p, Node.HORIZONTAL, newBound);
                }
            } else {
                if (p.y() < node.getPoint().y()) {
                    RectHV newBound = new RectHV(boundRect.xmin(), boundRect.ymin(), boundRect.xmax(), node.getPoint().y());
                    node.left = insert(node.left, p, Node.VERTICAL, newBound);
                } else {
                    RectHV newBound = new RectHV(boundRect.xmin(), node.getPoint().y(), boundRect.xmax(), boundRect.ymax());
                    node.right = insert(node.right, p, Node.VERTICAL, newBound);
                }
            }
        }

        return node;

    }

    // x1, y1, x2, y2 - bound rectangle for point
    private Node insert(Node node, Point2D p, boolean orientation, double x1, double y1, double x2, double y2) {
        if (node == null) {
            size++;
            return new Node(p, orientation, new RectHV(x1, y1, x2, y2));
        }

        if (!node.getPoint().equals(p)) {
            if (node.orientation == Node.VERTICAL) {
                if (p.x() < node.getPoint().x()) {
                    node.left = insert(node.left, p, Node.HORIZONTAL, x1, y1, node.getPoint().x(), y2);
                } else {
                    node.right = insert(node.right, p, Node.HORIZONTAL, node.getPoint().x(), y1, x2, y2);
                }
            } else {
                if (p.y() < node.getPoint().y()) {
                    node.left = insert(node.left, p, Node.VERTICAL, x1, y1, x2, node.getPoint().y());
                } else {
                    node.right = insert(node.right, p, Node.VERTICAL, x1, node.getPoint().y(), x2, y2);
                }
            }
        }

        return node;

    }

    // does the set contain point p?
    public boolean contains(Point2D p) {
        if (p == null) {
            throw new NullPointerException();
        }

        return contains(root, p);
    }

    private boolean contains(Node node, Point2D p) {
        if (node == null) return false;
        if (node.getPoint().equals(p)) return true;

        if (node.orientation == Node.VERTICAL) {
            if (p.x() < node.getPoint().x()) {
                return contains(node.left, p);
            } else {
                return contains(node.right, p);
            }
        } else {
            if (p.y() < node.getPoint().y()) {
                return contains(node.left, p);
            } else {
                return contains(node.right, p);
            }
        }
    }

    // draw all points to standard draw
    public void draw() {
        draw(root);
    }

    private void draw(Node node) {
        if (node == null) return;

        if (node.orientation == Node.VERTICAL) {
            //System.out.println("Ver: " + boundX1 + ", " + boundY1 + ", " + boundX2 + ", " + boundY2);
            StdDraw.setPenColor(StdDraw.BLACK);
            StdDraw.setPenRadius(0.01);
            node.getPoint().draw();
            StdDraw.setPenRadius();

            StdDraw.setPenColor(StdDraw.RED);
            node.getPoint().drawTo(new Point2D(node.getPoint().x(), node.boundRect.ymin()));
            node.getPoint().drawTo(new Point2D(node.getPoint().x(), node.boundRect.ymax()));
            draw(node.left);
            draw(node.right);
        } else {
            //System.out.println("Hor: " + boundX1 + ", " + boundY1 + ", " + boundX2 + ", " + boundY2);
            StdDraw.setPenColor(StdDraw.BLACK);
            StdDraw.setPenRadius(0.01);
            node.getPoint().draw();
            StdDraw.setPenRadius();

            StdDraw.setPenColor(StdDraw.BLUE);
            node.getPoint().drawTo(new Point2D(node.boundRect.xmin(), node.getPoint().y()));
            node.getPoint().drawTo(new Point2D(node.boundRect.xmax(), node.getPoint().y()));
            draw(node.left);
            draw(node.right);
        }

    }

    // all points that are inside the rectangle
    public Iterable<Point2D> range(RectHV rect) {
        if (rect == null) {
            throw new NullPointerException();
        }

        List<Point2D> list = new LinkedList<Point2D>();
        findRange(root, rect, list);

        return list;
    }

    private void findRange(Node node, RectHV rect, List<Point2D> resultList) {
        if (node == null) return;
        if (rect.contains(node.getPoint())) {
            resultList.add(node.getPoint());
        }

        if (!rect.intersects(node.getBoundRect())) {
            return;
        }

        if (node.orientation == Node.VERTICAL) {
            if (node.getPoint().x() <= rect.xmax()) {
                findRange(node.right, rect, resultList);
            }
            if (node.getPoint().x() >= rect.xmin()) {
                findRange(node.left, rect, resultList);
            }
        } else {
            if (node.getPoint().y() <= rect.ymax()) {
                findRange(node.right, rect, resultList);
            }
            if (node.getPoint().y() >= rect.ymin()) {
                findRange(node.left, rect, resultList);
            }
        }
    }

    // a nearest neighbor in the set to point p; null if the set is empty
    public Point2D nearest(Point2D p) {
        if (p == null) {
            throw new NullPointerException();
        }

        if (root == null) {
            return null;
        }

        Node closest = new Node(root.getPoint(), root.orientation, root.getBoundRect());
        getNearest(root, p, closest);

        return closest.getPoint();
    }

    private void getNearest(Node node, Point2D p, Node closest) {
        if (node == null) return;

        if (node.getPoint().distanceSquaredTo(p) < closest.getPoint().distanceSquaredTo(p)) {
            closest.setPoint(node.getPoint());
            closest.setBoundRect(node.getBoundRect());
        }

        if (closest.getPoint().distanceSquaredTo(p) < node.getBoundRect().distanceSquaredTo(p)) {
            return;
        }

        if (node.orientation == Node.VERTICAL) {
            if (p.x() < node.getPoint().x()) {
                getNearest(node.left, p, closest);
                getNearest(node.right, p, closest);
            } else {
                getNearest(node.right, p, closest);
                getNearest(node.left, p, closest);
            }
        } else {
            if (p.y() < node.getPoint().y()) {
                getNearest(node.left, p, closest);
                getNearest(node.right, p, closest);
            } else {
                getNearest(node.right, p, closest);
                getNearest(node.left, p, closest);
            }
        }

    }

    // unit testing of the methods (optional)
    public static void main(String[] args) {

        KdTree kdTree = new KdTree();

        kdTree.insert(new Point2D(0.48, 0.79)); //A
        kdTree.insert(new Point2D(0.5, 0.72));  //B
        kdTree.insert(new Point2D(0.78, 0.92)); //C
        kdTree.insert(new Point2D(0.57, 0.54)); //D
        kdTree.insert(new Point2D(0.41, 0.43)); //E
        kdTree.insert(new Point2D(0.84, 0.8));  //F
        kdTree.insert(new Point2D(0.88, 0.88)); //G
        kdTree.insert(new Point2D(0.72, 0.05)); //H

        System.out.println(kdTree.size());

        //false
        System.out.println(kdTree.contains(new Point2D(0.81, 0.8)));

        //true
        System.out.println(kdTree.contains(new Point2D(0.84, 0.8)));

        System.out.println(kdTree.toStr());

    }

    private String nodeToStr(Node node) {
        if (node == null) return "";

        String strLeft = nodeToStr(node.left);
        String strRight = nodeToStr(node.right);

        return node.getPoint().toString() + " " + strLeft + " " + strRight;
    }

    private String toStr() {
        return nodeToStr(root);
    }
}
