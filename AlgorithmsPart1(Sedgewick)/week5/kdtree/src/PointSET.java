import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

/**
 * Created by kaeltas on 27.02.15.
 */
public class PointSET {
    private Set<Point2D> set;

    // construct an empty set of points
    public PointSET() {
        set = new TreeSet<Point2D>();
    }

    // is the set empty?
    public boolean isEmpty() {
        return set.isEmpty();
    }

    // number of points in the set
    public int size() {
        return set.size();
    }

    // add the point to the set (if it is not already in the set)
    public void insert(Point2D p) {
        if (p == null) {
            throw new NullPointerException();
        }
        set.add(p);
    }

    // does the set contain point p?
    public boolean contains(Point2D p) {
        if (p == null) {
            throw new NullPointerException();
        }
        return set.contains(p);
    }

    // draw all points to standard draw
    public void draw() {
        for (Point2D p : set) {
            p.draw();
        }
    }

    // all points that are inside the rectangle
    public Iterable<Point2D> range(RectHV rect) {
        if (rect == null) {
            throw new NullPointerException();
        }

        List<Point2D> pointsInsideRect = new LinkedList<Point2D>();
        for (Point2D p : set) {
            if (rect.contains(p)) {
                pointsInsideRect.add(p);
            }
        }

        return pointsInsideRect;
    }

    // a nearest neighbor in the set to point p; null if the set is empty
    public Point2D nearest(Point2D p) {
        if (p == null) {
            throw new NullPointerException();
        }

        Point2D nearestPoint = null;
        for (Point2D setPoint : set) {
            if (nearestPoint == null) {
                nearestPoint = setPoint;
                continue;
            }
            if (setPoint.distanceTo(p) < nearestPoint.distanceTo(p)) {
                nearestPoint = setPoint;
            }
        }

        return nearestPoint;
    }

    // unit testing of the methods (optional)
    public static void main(String[] args) {

    }
}