import java.util.*;
import java.util.Stack;

/**
 * Created by kaeltas on 20.02.15.
 */
public class Solver {

    private MinPQ<SearchNode> minPq = new MinPQ<SearchNode>();
    private MinPQ<SearchNode> minPqTwin = new MinPQ<SearchNode>();
    private boolean isSolvable = false;
    private SearchNode solution;

    private class SearchNode implements Comparable<SearchNode> {
        private final Board board;
        private final int moves;
        private final SearchNode prevSearchNode;

        public SearchNode(Board board, int moves, SearchNode prevSearchNode) {
            this.board = board;
            this.moves = moves;
            this.prevSearchNode = prevSearchNode;
        }

        /*@Override
        public int compareTo(SearchNode o) {
            return (board.hamming()+moves) - (o.board.hamming()+o.moves);
        }*/

        @Override
        public int compareTo(SearchNode o) {
            return (board.manhattan()+moves) - (o.board.manhattan()+o.moves);
        }
    }


    // find a solution to the initial board (using the A* algorithm)
    public Solver(Board initial) {
        if (initial == null) throw new NullPointerException();

        minPq.insert(new SearchNode(initial, 0, null));
        minPqTwin.insert(new SearchNode(initial.twin(), 0, null));

        SearchNode searchNode = minPq.delMin();
        SearchNode searchNodeTwin = minPqTwin.delMin();
        while (!searchNode.board.isGoal() && !searchNodeTwin.board.isGoal()) {
            for (Board board : searchNode.board.neighbors()) {
                if (searchNode.prevSearchNode == null) {
                    minPq.insert(new SearchNode(board, searchNode.moves + 1, searchNode));
                } else if (!board.equals(searchNode.prevSearchNode.board)) {
                    minPq.insert(new SearchNode(board, searchNode.moves + 1, searchNode));
                }
            }

            for (Board board : searchNodeTwin.board.neighbors()) {
                if (searchNodeTwin.prevSearchNode == null) {
                    minPqTwin.insert(new SearchNode(board, searchNodeTwin.moves + 1, searchNodeTwin));
                } else if (!board.equals(searchNodeTwin.prevSearchNode.board)) {
                    minPqTwin.insert(new SearchNode(board, searchNodeTwin.moves + 1, searchNodeTwin));
                }
            }

            searchNode = minPq.delMin();
            searchNodeTwin = minPqTwin.delMin();

            /*System.out.println("-------");
            for (SearchNode s : minPqTwin) {
                System.out.println(s.board.manhattan()+s.moves);
                System.out.println(s.board);
            }*/
        }

        if (searchNode.board.isGoal()) {
            isSolvable = true;
            solution = searchNode;
        } else {
            isSolvable = false;
            solution = null;
        }




    }

    // is the initial board solvable?
    public boolean isSolvable() {
        return isSolvable;
    }

    // min number of moves to solve initial board; -1 if unsolvable
    public int moves() {
        if (isSolvable()) {
            return solution.moves;
        } else {
            return -1;
        }
    }

    // sequence of boards in a shortest solution; null if unsolvable
    public Iterable<Board> solution() {
        if (isSolvable()) {
            return new SolutionIterable();
        } else {
            return null;
        }
    }

    private class SolutionIterable implements Iterable<Board> {
        @Override
        public Iterator<Board> iterator() {
            return new SolutionIterator();
        }

        private class SolutionIterator implements Iterator<Board> {
            private Stack<Board> solutionBoards = new Stack<Board>();

            public SolutionIterator() {
                SearchNode solutionIter = solution;
                while (solutionIter != null) {
                    solutionBoards.push(solutionIter.board);
                    solutionIter = solutionIter.prevSearchNode;
                }
            }

            @Override
            public boolean hasNext() {
                return !solutionBoards.isEmpty();
            }

            @Override
            public Board next() {
                return solutionBoards.pop();
            }

            @Override
            public void remove() {
                throw new UnsupportedOperationException();
            }
        }
    }

    // solve a slider puzzle (given below)
    public static void main(String[] args) {
        // create initial board from file
        In in = new In(args[0]);
        //In in = new In("test-data/puzzle15.txt");
        int N = in.readInt();
        int[][] blocks = new int[N][N];
        for (int i = 0; i < N; i++)
            for (int j = 0; j < N; j++)
                blocks[i][j] = in.readInt();
        Board initial = new Board(blocks);

        // solve the puzzle
        Solver solver = new Solver(initial);

        // print solution to standard output
        if (!solver.isSolvable())
            StdOut.println("No solution possible");
        else {
            StdOut.println("Minimum number of moves = " + solver.moves());
            for (Board board : solver.solution())
                StdOut.println(board);
        }
    }
}