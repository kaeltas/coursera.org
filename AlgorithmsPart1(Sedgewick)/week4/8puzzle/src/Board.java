import java.util.*;

/**
 * Created by kaeltas on 20.02.15.
 */
public class Board {

    private final int N;
    private final int [][] blocks;

    // construct a board from an N-by-N array of blocks
    // (where blocks[i][j] = block in row i, column j)
    public Board(int[][] blocks) {
        N = blocks.length;

        int tmpCounter = 1;
        this.blocks = new int [N][N];
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N; j++) {
                this.blocks[i][j] = blocks[i][j];
            }
        }
    }

    // board dimension N
    public int dimension() {
        return N;
    }

    // number of blocks out of place
    public int hamming() {
        int countBlocksOutOfPlace = 0;
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N; j++) {
                if (blocks[i][j] != 0) {
                    int goalRow = (blocks[i][j]-1) / N;
                    int goalCol = (blocks[i][j]-1) % N;
                    if (goalRow != i || goalCol != j) {
                        countBlocksOutOfPlace++;
                    }
                }
            }
        }
        return countBlocksOutOfPlace;
    }

    // sum of Manhattan distances between blocks and goal
    public int manhattan() {
        int sumDistances = 0;
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N; j++) {
                if (blocks[i][j] != 0) {
                    int goalRow = (blocks[i][j]-1) / N;
                    int goalCol = (blocks[i][j]-1) % N;
                    sumDistances += abs(i - goalRow) + abs(j - goalCol);
                }
            }
        }
        return sumDistances;
    }

    private int abs(int val) {
        if (val < 0) {
            return val*(-1);
        }
        return val;
    }

    // is this board the goal board?
    public boolean isGoal() {
        return hamming() == 0;
    }

    // a board that is obtained by exchanging two adjacent blocks in the same row
    public Board twin() {
        int [][] auxBlocks = new int [N][N];
        boolean isExchanged = false;
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N; j++) {
                auxBlocks[i][j] = blocks[i][j];
                if (!isExchanged && j-1 >= 0 && blocks[i][j] != 0 && blocks[i][j-1] != 0) {
                    auxBlocks[i][j] = blocks[i][j-1];
                    auxBlocks[i][j-1] = blocks[i][j];
                    isExchanged = true;
                }
            }
        }

        return new Board(auxBlocks);
    }

    // does this board equal y?
    public boolean equals(Object that) {
        if (that == null) return false;
        if (that == this) return true;

        if (that.getClass() != this.getClass()) return false;

        Board thatBoard = (Board) that;
        //check dimension
        if (thatBoard.dimension() != dimension()) return false;
        //check equality of all elements
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N; j++) {
                if (thatBoard.blocks[i][j] != blocks[i][j]) return false;
            }
        }

        return true;
    }

    // all neighboring boards
    public Iterable<Board> neighbors() {
        return new Neighbours();
    }

    private class Neighbours implements Iterable<Board> {
        private List<Board> neighbours = new ArrayList<Board>();

        private class NeighboursIterator implements Iterator<Board> {
            private int index = 0;

            @Override
            public boolean hasNext() {
                return index < neighbours.size();
            }

            @Override
            public Board next() {
                return neighbours.get(index++);
            }

            @Override
            public void remove() {
                throw new UnsupportedOperationException();
            }
        }

        public Neighbours() {

            //find blank block
            int blankRow = 0;
            int blankCol = 0;
            outerFor:
            for (int i = 0; i < N; i++) {
                for (int j = 0; j < N; j++) {
                    if (blocks[i][j] == 0) {
                        blankRow = i;
                        blankCol = j;
                        break outerFor;
                    }
                }
            }

            //check 4 neighbours
            //top
            if (blankRow > 0) {
                int[][] auxBlocks = new int[N][N];
                for (int i = 0; i < N; i++) {
                    for (int j = 0; j < N; j++) {
                        auxBlocks[i][j] = blocks[i][j];
                    }
                }

                int tmp = auxBlocks[blankRow][blankCol];
                auxBlocks[blankRow][blankCol] = auxBlocks[blankRow-1][blankCol];
                auxBlocks[blankRow-1][blankCol] = tmp;

                neighbours.add(new Board(auxBlocks));
            }

            //bottom
            if (blankRow < N-1) {
                int[][] auxBlocks = new int[N][N];
                for (int i = 0; i < N; i++) {
                    for (int j = 0; j < N; j++) {
                        auxBlocks[i][j] = blocks[i][j];
                    }
                }

                int tmp = auxBlocks[blankRow][blankCol];
                auxBlocks[blankRow][blankCol] = auxBlocks[blankRow+1][blankCol];
                auxBlocks[blankRow+1][blankCol] = tmp;

                neighbours.add(new Board(auxBlocks));
            }

            //left
            if (blankCol > 0) {
                int[][] auxBlocks = new int[N][N];
                for (int i = 0; i < N; i++) {
                    for (int j = 0; j < N; j++) {
                        auxBlocks[i][j] = blocks[i][j];
                    }
                }

                int tmp = auxBlocks[blankRow][blankCol];
                auxBlocks[blankRow][blankCol] = auxBlocks[blankRow][blankCol-1];
                auxBlocks[blankRow][blankCol-1] = tmp;

                neighbours.add(new Board(auxBlocks));
            }

            //right
            if (blankCol < N-1) {
                int[][] auxBlocks = new int[N][N];
                for (int i = 0; i < N; i++) {
                    for (int j = 0; j < N; j++) {
                        auxBlocks[i][j] = blocks[i][j];
                    }
                }

                int tmp = auxBlocks[blankRow][blankCol];
                auxBlocks[blankRow][blankCol] = auxBlocks[blankRow][blankCol+1];
                auxBlocks[blankRow][blankCol+1] = tmp;

                neighbours.add(new Board(auxBlocks));
            }
        }

        @Override
        public Iterator<Board> iterator() {
            return new NeighboursIterator();
        }

    }

    // string representation of this board (in the output format specified below)
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(dimension());
        sb.append("\n");
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N; j++) {
                sb.append(" " + blocks[i][j] + " ");
            }
            sb.append("\n");
        }

        return sb.toString();
    }


    // unit tests (not graded)
    public static void main(String[] args) {
        // create initial board from file
        In in = new In("test-data/puzzle07.txt");
        int N = in.readInt();
        int[][] blocks = new int[N][N];
        for (int i = 0; i < N; i++)
            for (int j = 0; j < N; j++)
                blocks[i][j] = in.readInt();
        Board initial = new Board(blocks);
        System.out.println(initial.hamming());
        System.out.println(initial.manhattan());
        System.out.println(initial);

        System.out.println("Neighbours");
        for (Board board : initial.neighbors()) {
            System.out.println(board);
        }
    }
}