/**
 * Created by kaeltas on 02.02.15.
 */
public class Percolation {
    private static int siteSize = 20;
    private static int pad = 10;

    private int N;
    private boolean [][]sites;

    //QuickFindUF uf;
    private WeightedQuickUnionUF uf;
    private WeightedQuickUnionUF ufHandleBackwash;
    private int virtualTopSiteIndex;
    private int virtualBottomSiteIndex;


    // create N-by-N grid, with all sites blocked
    public Percolation(int N) {
        if (N <= 0) {
            throw new IllegalArgumentException();
        }
        this.N = N;

        sites = new boolean[N][N];
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N; j++) {
                sites[i][j] = false;
            }
        }

        //uf = new QuickFindUF(N*N+2);
        uf = new WeightedQuickUnionUF(N*N+2);
        ufHandleBackwash = new WeightedQuickUnionUF(N*N+1);

        virtualTopSiteIndex = N*N;
        virtualBottomSiteIndex = N*N+1;

        for (int i = 0; i < N; i++) {
            uf.union(i, virtualTopSiteIndex); //connect top row to virtualTopSite
            ufHandleBackwash.union(i, virtualTopSiteIndex); //connect top row to virtualTopSite
            uf.union(N*(N-1)+i, virtualBottomSiteIndex); //connect bottom row to virtualBottomSite
        }

    }

    // open site (row i, column j) if it is not open already
    public void open(int i, int j) {
        if (i <= 0 || j <= 0 || i > N || j > N) {
            throw new IndexOutOfBoundsException();
        }

        sites[j-1][i-1] = true;
        //check 4 neighbours. If one is open - connect new site to it.
        //Top
        if (i-2 >= 0 && sites[j-1][i-2]) {
            uf.union((j-1)+ N *(i-1), (j-1)+ N *(i-2));
            ufHandleBackwash.union((j - 1) + N * (i - 1), (j - 1) + N * (i - 2));
        }
        //Bottom
        if (i < N && sites[j-1][i]) {
            uf.union((j-1)+ N *(i-1), (j-1)+ N *(i));
            ufHandleBackwash.union((j - 1) + N * (i - 1), (j - 1) + N * (i));
        }
        //Left
        if (j-2 >= 0 && sites[j-2][i-1]) {
            uf.union((j-1)+ N *(i-1), (j-2)+ N *(i-1));
            ufHandleBackwash.union((j - 1) + N * (i - 1), (j - 2) + N * (i - 1));
        }
        //Right
        if (j < N && sites[j][i-1]) {
            uf.union((j-1)+ N *(i-1), (j)+ N *(i-1));
            ufHandleBackwash.union((j - 1) + N * (i - 1), (j) + N * (i - 1));
        }

    }

    // is site (row i, column j) open?
    public boolean isOpen(int i, int j) {
        if (i <= 0 || j <= 0 || i > N || j > N) {
            throw new IndexOutOfBoundsException();
        }

        return sites[j-1][i-1];
    }

    // is site (row i, column j) full?
    public boolean isFull(int i, int j) {
        if (i <= 0 || j <= 0 || i > N || j > N) {
            throw new IndexOutOfBoundsException();
        }

        return isOpen(i, j) && ufHandleBackwash.connected((j-1)+(N)*(i-1), virtualTopSiteIndex);
    }

    // does the system percolate?
    public boolean percolates() {
        if (N == 1) {
            return isOpen(1, 1);
        } else {
            return uf.connected(virtualBottomSiteIndex, virtualTopSiteIndex);
        }
    }


    private void draw() {
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N; j++) {
                if (isFull(i+1, j+1)) {
                    //System.out.println("Full " + (i+1) + "," + (j+ 1));
                    StdDraw.setPenColor(StdDraw.BOOK_LIGHT_BLUE);
                    StdDraw.filledRectangle(2*pad+j*siteSize, 2*pad+(N -1-i)*siteSize, siteSize/2-1, siteSize/2-1);
                } else if (isOpen(i+1, j+1)) {
                    StdDraw.setPenColor(StdDraw.WHITE);
                    StdDraw.filledRectangle(2*pad+j*siteSize, 2*pad+(N -1-i)*siteSize, siteSize/2-1, siteSize/2-1);
                } else {
                    StdDraw.setPenColor(StdDraw.DARK_GRAY);
                    StdDraw.filledRectangle(2*pad+j*siteSize, 2*pad+(N -1-i)*siteSize, siteSize/2-1, siteSize/2-1);
                }
            }
        }
    }


    // test client (optional)
    public static void main(String[] args) {

        /*Percolation percolation = new Percolation(20);

        int countOpened = 0;
        while (!percolation.percolates()) {
            int rndI = 1 + StdRandom.uniform(percolation.N);
            int rndJ = 1 + StdRandom.uniform(percolation.N);

            if (!percolation.isOpen(rndI, rndJ)) {
                countOpened++;
            }
            percolation.open(rndI, rndJ);

        }

        System.out.println(Double.valueOf(countOpened)/Double.valueOf(percolation.N*percolation.N));
        System.out.println(percolation.percolates());


        if (percolation.N < 30) {
            StdDraw.setXscale(0, siteSize * percolation.N + 2 * pad);
            StdDraw.setYscale(0, siteSize * percolation.N + 2 * pad);
            for (int i = 0; i <= percolation.N; i++) {
                StdDraw.line(pad + i * siteSize, pad, pad + i * siteSize, pad + siteSize * percolation.N);
                StdDraw.line(pad, pad + i * siteSize, pad + percolation.N * siteSize, pad + i * siteSize);
            }
            percolation.draw();
        }*/


    }
}
