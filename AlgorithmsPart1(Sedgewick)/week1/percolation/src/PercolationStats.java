import java.util.Arrays;

/**
 * Created by kaeltas on 02.02.15.
 */
public class PercolationStats {

    //private int N;
    private int T;
    private double [] openedSitesFraction;

    // perform T independent experiments on an N-by-N grid
    public PercolationStats(int N, int T) {
        if (N <= 0 || T <= 0) {
            throw new IllegalArgumentException();
        }

        //this.N = N;
        this.T = T;
        openedSitesFraction = new double[T];

        for (int i = 0; i < T; i++) {
            Percolation p = new Percolation(N);

            int countOpened = 0;
            while (!p.percolates()) {
                int rndI = 1 + StdRandom.uniform(N);
                int rndJ = 1 + StdRandom.uniform(N);

                if (!p.isOpen(rndI, rndJ)) {
                    countOpened++;
                }
                p.open(rndI, rndJ);
            }

            openedSitesFraction[i] = Double.valueOf(countOpened)/Double.valueOf(N*N);
        }
    }

    // sample mean of percolation threshold
    public double mean() {
        return StdStats.mean(openedSitesFraction);
    }

    // sample standard deviation of percolation threshold
    public double stddev() {
        return StdStats.stddev(openedSitesFraction);
    }

    // low  endpoint of 95% confidence interval
    public double confidenceLo() {
        return mean() - 1.96*stddev()/Math.sqrt(T);
    }

    // high endpoint of 95% confidence interval
    public double confidenceHi() {
        return mean() + 1.96*stddev()/Math.sqrt(T);
    }

    // test client (described below)
    public static void main(String[] args) {
        int N = Integer.valueOf(args[0]);
        int T = Integer.valueOf(args[1]);
        /*int N = 200;
        int T = 100;*/
        PercolationStats percolationStats = new PercolationStats(N, T);

        System.out.println("mean = " + percolationStats.mean());
        System.out.println("stddev = " + percolationStats.stddev());
        System.out.println("95% confidence interval = " + percolationStats.confidenceLo() + ", " + percolationStats.confidenceHi());

    }
}
